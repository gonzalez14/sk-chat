# Secret-Key Chat (sk-chat)
A solution for USF's CIS-4930 "Computer Networks and Security" final project.

This application is designed to implement a "secure" chat program given the
constraint that one is to utilize only 56-bit keys.

The basic design document can currently be found under `docs/design.md`. A more
comprehensive whitepaper should be available upon project completion.

## Building the tool
A CMake build script is provided, allowing the project to be built simply by:

```
mkdir build
cd build
cmake ..
make
cd ..
```

The program will then be available as the generated `build/sk-chat` executable.

### Dependencies
* Qt::Widgets
* Boost::(Beast, Asio, Log)
* Verdigris †
* Nlohmann JSON †
* Crypto++ †

† - *Dependencies are automatically resolved by the build script.*
