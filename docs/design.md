# CIS 6930 - Computer Security

## Overview
Secure messaging app between 2-parties over an arbitrarily secure network.
Key size of only **56-bits** using **DES-CBC** encryption, which necessitates
key tumbling. Due to the weakness of the key, the project becomes an endeavor in
minimizing penetration depth rather than preventing penetration at all.

We authenticate both sides using **Scrypt** password hashes and a
challenger-provided nonce. A secondary hash (using the same nonce) of only
128-bits is used to derive the Master and initial Aux keys. The Master key,
$K_m$, is used for encryption and the Auxiliary key, $K_a$, is used to generate
the integrity digests for messages. After some $n$ messages, the keys are
securely tumbled.

Each side of the communication has it's own **one-way values** such as keys,
running digests, etc and only share the initial password and nonces. This allows
us to circumvent the complexity of synchronizing states for key tumbling
purposes.

Never use this tool in the real world. Nothing short of 256-bit key encryption
should be used, preferably making use of asymmetric keys for key-exchange.

## Dependencies
### Boost::Log
URL:
License: Boost
Purpose: General purpose application logging.

### Boost::Asio
URL:
License: Boost
Purpose: Asynchronous I/O TCP/UDP sockets.

### Boost::Beast
URL:
License: Boost
Purpose: Websocket abstraction over Boost::Asio sockets with message framing.

### Qt::Widgets
URL:
License: LGPL
Purpose: General purpose GUI framework.

### Verdigris
URL:
License:
Purpose: Allows us to use Qt slots and signals without the Qt MOC.

### JSON
URL:
License:
Purpose: Simple, modern C++ support for JSON table (de)serialization.

### Crypto++
URL:
License:
Purpose: Provides the cryptographic primitives (DES, CBC, Blake2B, Scrypt).

## The Interface
```plantuml
@startuml
salt
{+
  {* File | Chat }
  {/ Dave | Friend2 | ...}
  {
    {
      {+
        Connection Established!
        ----
        ----
        Dave
        ----
        What is up my dude?<&tag>

        ----
        Me
        ----
        Not much my dude!<&tag>
      }
      ----
      {
        "Type your message here." | [Send]
      }
    } | {+
      Connection Established
      Received message <DIGEST>... <&check>
      Tumbling Aux_In... <&check>
      Sending message... <&check>
      ----
      {
        Password Digest: | XXXXXXXXXXXXXXXX
        Running Hash:    | XXXXXX & XXXXXX
        Master Key:      | XXXXX  & XXXXX
        Current Aux:     | XXXXX  & XXXXX
        Block #:         | $$$$   & $$$$
      }
    }
  }
}
@enduml
```

### Starting New Chats
We make a request to start a new chat by selecting `Chat > New`. The
dialogue below will be opened.

```plantuml
@startuml
salt
{+
  Target IP: | "127.0.0.1"
  Username:  | "FigNewton"
  Password:  | "*********"
  [Cancel]   | [Connect]
}
@enduml
```

The user should enter an IP address for the second party, a pseudonym that they
would like to use during the chat, and the password they would like to use.
Once established, a new TCP port is reserved and the new chat created in a
handshake state.

The other party receives this request and the following dialogue is presented:

```plantuml
@startuml
salt
{+
  Incoming Chat Request
  IP:       | 192.168.0.1
  Port:     | 65356
  Username: | FigNewton
  Nonce:    | XXXXXXX
  [Reject]  | [Accept]
}
@enduml
```

They may choose to reject or accept the message. In the case of the former, the
program simply sends the refusal and the dialogue is closed, in the case of the
latter the same "new chat" dialogue is provided (with the IP field grayed out).

After pressing "Connect" the initial party is presented one last incoming
chat request dialog to confirm or deny. If accepted, the handshake is performed
and one of the following prompts displayed.

#### Prompt on Failure
```plantuml
@startuml
salt
{+
  <&warning> Connection Failed
  ----
  <REASON>
  ----
  [Ok]
}
@enduml
```

The failures that bring this prompt are non-security related (could not
establish connection, manual refusal, etc).

#### Prompt on Security Failure
```plantuml
@startuml
salt
{+
  <&shield> Digest Refused
  ----
  User and Partner passwords do not match.
  ----
  XXXXXXXXXXXXXXXX / XXXXXXXXXXXXXXXX
  ----
  [Ok]
}
@enduml
```

This prompt is thrown when digests are non-matching. It is *extra* important
that any time this occurs, the nonces used are saved and *never used again*.

#### Prompt on Good Connection
```plantuml
@startuml
salt
{+
  <&check> Connection Secure
  ----
  [Ok]
}
@enduml
```

Yay, you're all set up!

### Closing a Chat
Simply click `Chat > Close`

### Exiting the Program
Simply click `File > Quit`

## Algorithms
### Authentication Digest
$AuthDigest = Scrypt(P, S, t, c, b, p)$

Scrypt is sufficiently strong on it's own for this purpose, and there is no need
to complicate it further.

### (Master, Aux) Key Generation
$(K_m | K_0) = Scrypt(P, S, 16, c, b, p)$

Again, Scrypt is more than sufficient for this task. There is no opportunity for
the attacker to recover information about the password or key pairs based on the
prior authentication digest.

**Note**: If the resulting keys are "weak" - they need to go through key
tumbling until strong.

### Master Key Tumbling
$K_m = Scrypt(P | K_{a-1} | ((K_{m-1}) | D_n), (S + n_b), 8, c, b, p)$
$K_a = Scrypt(P | K_{m-1} | ((K_{a-1}) | D_n), (S + n_b), 8, c, b, p)$

We use Scrypt again for simplicity. The password acts as the base,
"strengthened" with the Auxiliary key, the Master key, and the current running
digest of all messages. The salt is also adjusted by the current block number.

In this way, an attacker essentially needs to break every block individually as
well as recover the original secret password. The amount of information
necessary to tumble any current key is unfeasible for them to recover and any
attacker must fall-back to brute-forcing. Scrypt is computationally/memory-hard,
so prior to this an attacker would simply attack the weak 56-bit keys directly
(approximately 22 hours per key on state-of-the-art hardware).

**Note**: If any of the resulting key are "weak" - they must be rejected and
sent through further tumbling until strong.

### Message Digest
$Digest = Blake2B(M, K_a)$

Keyed Blake2B is secure and extremely fast, so this is perfect for our use case.
Due to the small key size, brute-forcing is a viable attack. However, for one to
be able to spoof messages, they must break both the master encryption key and
the auxiliary validation key separately. This is considered "sufficiently
difficult" for the project at hand - but in a real-world context would require
a significantly larger key size.

### Connection Handshake
```plantuml
@startuml
hide footbox

create Bob_Room
== Dispatch ==
Bob_Dispatch -> Bob_Room : Dispatch port 0
Bob_Dispatch -> Alice_Dispatch : Request for Chat, N2, Port, Alias
create Alice_Room
Alice_Dispatch -> Alice_Room : Dispatch port 0

== Validation ==
Alice_Room -> Bob_Room : Challenge, N1, Alias
Bob_Room -> Alice_Room : Accept, H(N1 | P | N1)
hnote over Alice_Room : Validate
Alice_Room -> Bob_Room : Accept, H(N2 | P | N2)
hnote over Bob_Room : Validate
Bob_Room -> Alice_Room : Secure Accept
Alice_Room -> Bob_Room : Secure Accept

== Connection Established ==
@enduml
```

This still isn't quite ideal as the aliases are transmitted over plaintext, but
this is an intentional choice to make the user's life a little easier when it
comes to establishing connections.

### Message Exchange
The following diagram outlines the simple means of a successful message
exchange.

```plantuml
@startuml
Bob_Room -> Alice_Room : Message, n, M, D
hnote over Alice_Room : Validate
Alice_Room -> Bob_Room : Good, n, D
@enduml
```

If the message fails to validate, though, we get one of theses:

```plantuml
@startuml
Bob_Room -X Alice_Room : Message, n, M, D
hnote over Alice_Room : Validate
Alice_Room -> Bob_Room : Bad, n
Bob_Room -> Alice_Room : Message, n, M, D
hnote over Alice_Room : Validate
note right of Bob_Room
Up to max attempts.
end note
@enduml
```

Occasionally, if a party has not recently sent a message their client will send
a keep-alive signal.

There is no need to handle the timeout case, as the TCP stream we use to
communicate is reliable in itself.

## Forms
Forms are sent as strings representing a JSON table.

### Bad Form
```
{
  "form": "bad-form"
}
```

### Request for Chat
```
{
  "form": "chat-request",
  "alias": "<NAME>",
  "port": "<PORT>",
  "nonce": "<RANDOM>"
}
```

### Challenge
```
{
  "form": "challenge",
  "nonce": "<RANDOM>",
  "alias": "<NAME>"
}
```

### Accept Chat Request
```
{
  "form": "accept-request",
  "digest": "<DIGEST>"
}
```

### Reject Chat Request
```
{
  "form": "reject-request",
  "why": "<CAUSE>"
}
```

### Secure Chat Acknowledge
```
{
  "form": "secure-ack",
  "secure": {
    "in-digest": "<IN-DIGEST>"
    "out-digest": "<OUT-DIGEST>"
  }
}
```

### Message
```
{
  "form": "message",
  "secure": {
    "message": "<MSG>",
    "n": "<N>",
    "digest": "<MSG-DIGEST>"
  }
}
```

### Good Message
```
{
  "form": "good-message",
  "secure" : {
    "n": "<N>,
    "digest": "<MSG-DIGEST>
  }
}
```

### Bad Message
```
{
  "form": "bad-message",
  "secure": {
    "n": "<N>",
    "digest": "MSG-DIGEST"
  }
}
```

### Keep Alive
```
{
  "form": "keep-alive"
}
```

## Architecture
