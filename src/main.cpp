#include <iostream>

#include <boost/beast.hpp>
#include <boost/endian/conversion.hpp>

#include <QApplication>
#include <QMainWindow>
#include <QObject>

#include <verdigris/wobjectdefs.h>

#include <nlohmann/json.hpp>

#include <cryptopp/cryptlib.h>

/**
 * QApplication
 * - QMainWindow
 *   - QMenuBar
 *     - Chatrooms
 *       - Open New Chatroom > NewChatDialogue
 *       - Chatroom Details > ExploreChatDialogue
 *       - Close Chatroom
 *     - Preferences > PreferenceEditDialogue
 *     - About > AboutUsDialogue
 *   - QHLayout
 *     - QTabWidget
 *       - QVLayout (Message Hidtory)
 *        - [QTextEdit (Message / Header)]
 *     - QHLayout
 *       - QTextEdit (Message editor)
 *       - QButton (Send button))
 */

int main(int argc, char **argv) {
  std::cout << "Hello world!" << std::endl;

  QApplication app{argc, argv};
  QMainWindow win{};
  win.show();
  return app.exec();
}
